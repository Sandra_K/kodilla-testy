VENV := venv
SYS_PYTHON := python3.10
PYTHON := $(VENV)/bin/python


all: venv add-poetry install test
	@echo "DONE"

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

venv: ## Make a new virtual environment if not exists
	IF NOT EXIST venv $(SYS_PYTHON) -m venv $(VENV)
	$(PYTHON) -m pip install --upgrade pip

	@echo "Activate venv"
	. venv/bin/activate

clean-venv: ## Remove virtualenv
	rm -rf venv

add-poetry: ## Install poetry
	pip install poetry

install: ## Install dependencies from poetry pyproject.toml
	@echo "Installing dependencies from poetry pyproject.toml"
	poetry install

test: ## Run tests
	poetry run pytest -vv

sort: ## Run isort
	poetry run isort .

format: ## Run black
	poetry run black .