import pydantic
import pytest
from pydantic.error_wrappers import ValidationError

from zad_1 import GameTicTac, tic_tac_toe_winner


@pytest.mark.parametrize(
    "game, expected",
    [
        pytest.param(GameTicTac(game="xxoxooxox"), "x(o)"),
        pytest.param(GameTicTac(game="xoooxoxox"), "x(o)"),
        pytest.param(GameTicTac(game="xoxoxoxoo"), "x(o)"),
        pytest.param(GameTicTac(game="xooooxxxx"), "x(o)"),
        pytest.param(GameTicTac(game="oooxxoxox"), "o(x)"),
        pytest.param(GameTicTac(game="oxoxoxxxo"), "o(x)"),
        pytest.param(
            GameTicTac(game="oxoxoxoxo"), None, id="O wins diagonally two times."
        ),
        pytest.param(GameTicTac(game="xoxxoooxo"), None),
        pytest.param(
            GameTicTac(game="oxoxooxxo"),
            None,
            id="O wins two times, so it will return None",
        ),
    ],
)
def test_tic_tac_toe_valid_data(game, expected):
    result = tic_tac_toe_winner(game)
    assert result == expected


@pytest.mark.parametrize(
    "game",
    [
        pytest.param("xxx"),
        pytest.param("xxxxxoxox"),
        pytest.param("oooxoxxoxxoxox"),
        pytest.param("aaaaaaaaa"),
        pytest.param("123456789"),
        pytest.param(["a", 1, 2, 3, 4, 5, 6, 7, 8]),
    ],
)
def test_tic_tac_toe_invalid_data_raise_exception(game):
    with pytest.raises(ValidationError):
        game = GameTicTac(game=game)
