from unittest import result

import pytest
from pydantic.error_wrappers import ValidationError as PydanticValidationError

from zad_2 import PalindromeData, is_palindrome


@pytest.mark.parametrize(
    "data, expected",
    [
        pytest.param("ala", True),
        pytest.param("madam", True),
        pytest.param("kajak", True),
        pytest.param("oko", True),
        pytest.param("123321", True),
        pytest.param("02-22-20", True),
        pytest.param("abc", False),
        pytest.param("10/10/99", False),
        pytest.param("---", False),
    ],
)
def test_is_palindrome_valid_data(data, expected):
    palindrome_data = PalindromeData(data=data)
    result = is_palindrome(palindrome_data)
    assert result == expected


@pytest.mark.parametrize(
    "data",
    [
        pytest.param("a"),
        pytest.param({"a": 0}),
    ],
)
def test_is_palindrome_raise_exception(data):
    with pytest.raises(AttributeError):
        result = is_palindrome(data)
