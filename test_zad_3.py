from zad_3 import prime_factors

def test_import_prime_factors():
    try:
        from zad_3 import prime_factors
        assert callable(prime_factors), "prime_factors not callable"
    except ImportError as err:
        assert False, err

def test_prime_factors_with_space():
    try:
        prime_factors(' ')
        assert False, "ValueError expected"
    except ValueError:
        pass

def test_prime_factors_4():
    result = prime_factors(4)
    assert result == [2, 2], f"expected list [2, 2], got {result}"

if __name__ == "__main__":
    try:
        for test in (
        test_import_prime_factors,
        test_prime_factors_with_space,
        test_prime_factors_4,
    ):
            print(f'{test.__name__}: ', end='')
            test()
            print("ok")
    except AssertionError as error:
        print(error)
