from typing import Dict, List

from pydantic import BaseModel, Field, error_wrappers, validator


class GameTicTac(BaseModel):
    game: str

    @validator("game")
    def length_occurrence(cls, game):
        if len(game) < 7:
            raise ValueError(f"Game's length should be 9 instead of {len(game)}.")
        occurrence_x = game.count("x")
        occurrence_o = game.count("o")
        if (occurrence_x not in [4, 5]) or (occurrence_o not in [4, 5]):
            raise ValueError(
                "The number of x/o in the GameTicTac should not be greater than 5 and less than 4."
            )
        return game


class GameResults(BaseModel):
    results: Dict[str, List[str]] = Field(..., alias="results")


def tic_tac_toe_winner(game_tic_tac: GameTicTac) -> str | None:
    """
    Ćwiczenie #1
    Napisz funkcję tic_tac_toe_winner,
    która sprawdza kto wygrał klasyczny wariant gry
    w kółko i krzyżyk na podstawie widoku planszy 3x3 pola.
    Plansza podawana jest jako ciąg 9 znaków X
    lub O oraz spacji. Funkcja powinna zwracać X (Y),
    gdy wygrał grający odpowiednio X (Y),
    oraz None, kiedy nie można tego rozstrzygnąć."""

    game = game_tic_tac.dict()
    game = game["game"]
    options = ["x", "o"]

    game_results = GameResults(
        results={
            "horizontally": [game[:3], game[3:6], game[6:]],
            "diagonally": [
                f"{game[0]}{game[4]}{game[8]}",
                f"{game[2]}{game[4]}{game[6]}",
            ],
            "upright": [
                f"{game[0]}{game[3]}{game[6]}",
                f"{game[1]}{game[4]}{game[7]}",
                f"{game[2]}{game[5]}{game[8]}",
            ],
        }
    )
    winner = []
    game_dict = game_results.dict()
    game_values = game_dict["results"].values()
    for option in options:
        for results in game_values:
            for result in results:
                if len(set(result)) == 1 and result[0] == option:
                    if option in options:
                        options.remove(option)
                    winner.append(f"{option}({options[0]})")
    if len(winner) == 1:
        return winner[0]
    return None
