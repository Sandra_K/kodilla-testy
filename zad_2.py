from pydantic import BaseModel, NonNegativeInt, validator
from pydantic.error_wrappers import ValidationError as PydanticValidationError


class PalindromeData(BaseModel):
    data: str | NonNegativeInt

    @validator("data", pre=True)
    def len_data(cls, data):
        if len(data) == 0:
            raise ValueError("Please provide valid data's len.")
        return data


def is_palindrome(palindrome_data: PalindromeData) -> bool:

    palindrome = palindrome_data.dict()
    data = palindrome["data"]

    if isinstance(data, int):
        data = str(data)

    alphanumeric = [
        character for character in data if character.isalnum() or character.isalpha()
    ]
    alphanumeric = "".join(alphanumeric)
    return alphanumeric == alphanumeric[::-1]
