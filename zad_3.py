from functools import wraps
from pydantic import BaseModel, validator
from sympy import primerange



def validate_number(function):

    @wraps(function)
    def wrapper(number):
        if type(number) != int:
            raise ValueError('Please provide valid number type: int.')
        return function(number)

    return wrapper

@validate_number
def prime_factors(number):

    odds = primerange(2, number)
    odds = list(odds)
    factors = []
    current = number
    index = 0
    for i in range(number+1):
        if current == 1:
            return factors
        elif current % odds[index] == 0:
            current = current / odds[index]
            factors.append(odds[index])
        else:
            index += 1
            continue
    return factors
